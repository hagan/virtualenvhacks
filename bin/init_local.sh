#!/bin/bash
echo "running init_local.sh.."
# >>>>>>>>>>>>DO NOT USE THIS SCRIPT UNLESS YOU KNOW WHAT IT DOES<<<<<<<<<<<<<<
if [[ ! -z "$BASH_SOURCE" ]]; then
  my_dir="$(dirname "$BASH_SOURCE")"
else
  my_dir="$(dirname "$0")"
fi
source "$my_dir/../envconfig.sh"
## Create our var directory if it does not exist yet
[ -d $(dirname ${VIRTUALENV_STATUS}) ] || mkdir $(dirname ${VIRTUALENV_STATUS})
## Create our virtualenv file if it does not exist yet
[ -e ${VIRTUALENV_STATUS} ] || touch ${VIRTUALENV_STATUS}
source ${VIRTUALENV_STATUS}
eval DEREF_CNTR_VAR=\$${ENV_CNTR_VAR}
if [ -z "${DEREF_CNTR_VAR}" ]; then
  export $ENV_CNTR_VAR=1
  eval DEREF_CNTR_VAR=\$${ENV_CNTR_VAR}
#   # Save that to our file!
  declare -p ${ENV_CNTR_VAR} | cut -d " " -f 3- > ${VIRTUALENV_STATUS}
  echo "1st virtual_env: active count: ${DEREF_CNTR_VAR}"
  ## Now setup our local private network
  if [[ "${OSTYPE}" =~ ^darwin.*$ ]]; then
    echo "detected osx os type"
    if [ "`ifconfig en0 | grep ${SRV_IP}`xxx" = "xxx" ]; then
      echo "!!!Adding private ip: ${SRV_IP}!!!"
      sudo ifconfig en0 inet alias ${SRV_IP} netmask ${NET_MASK}
      sudo ipfw add ${IPRULENUM} fwd ${SRV_IP},${SRV_PORT} tcp from any to any 80 in
    else
        echo "did not setup anything."
    fi
  elif [[ "${OSTYPE}" =~ ^freebsd.*$ ]]; then
    echo "detected freebsd"
     if [ "`ifconfig em0 | grep ${SRV_IP}`xxx" = "xxx" ]; then
      echo "!!!Adding private ip: ${SRV_IP}!!!"
      sudo ifconfig em0 inet alias ${SRV_IP} netmask ${NET_MASK}
      sudo ipfw add ${IPRULENUM} fwd ${SRV_IP},${SRV_PORT} tcp from any to any 80 in
    fi
    #statements
  echo "got here too!"
  elif [[ "${OSTYPE}" =~ ^linux.*$ ]]; then
    echo "detected linux.."
    echo "sudo ip address add $SRV_IP/$NET_MASK broadcast $NET_BROADCAST dev $NET_DEV label $NET_LABEL"
    sudo ip address add $SRV_IP/$NET_MASK broadcast $NET_BROADCAST dev $NET_DEV label $NET_LABEL

  fi
else
    echo "Already ran init script!"
    export ${ENV_CNTR_VAR}=$((${DEREF_CNTR_VAR} + 1 ))
    eval DEREF_CNTR_VAR=\$${ENV_CNTR_VAR}
    declare -p ${ENV_CNTR_VAR} | cut -d " " -f 3- > ${VIRTUALENV_STATUS}
    echo "Already running another virtual_env, now active: ${DEREF_CNTR_VAR}"
fi
###############################################################################