#!/bin/bash
echo "running deinit_local.sh.."
# >>>>>>>>>>>>DO NOT USE THIS SCRIPT UNLESS YOU KNOW WHAT IT DOES<<<<<<<<<<<<<<
if [[ ! -z "$BASH_SOURCE" ]]; then
    my_dir="$(dirname "$BASH_SOURCE")"
else
    my_dir="$(dirname "$0")"
fi
source "$my_dir/../envconfig.sh"
source ${VIRTUALENV_STATUS}
eval DEREF_CNTR_VAR=\$${ENV_CNTR_VAR}
if [ -z "${DEREF_CNTR_VAR}" ] || [ -z "${ENV_CNTR_VAR}" ]; then
    echo "ERROR: Somehow your environment is missing some key variables."
    exit
elif [ "${DEREF_CNTR_VAR}" -eq 1 ]; then
  ## delete the file?
  echo "No more active virtual_env, removing ${VIRTUALENV_STATUS} file."
  rm ${VIRTUALENV_STATUS}
  unset ${ENV_CNTR_VAR}
  unset ENV_CNTR_VAR
  echo "ostype: ${OSTYPE}"
  if [[ "${OSTYPE}" =~ ^darwin.*$ ]]; then
      echo "detected osx os.."
      ##OSX
      if [[ "`ifconfig en0 | grep ${SRV_IP}`xxx" != "xxx" ]]; then
        echo "Removing private ip: ${SRV_IP}"
        sudo ifconfig en0 inet -alias ${SRV_IP} netmask ${NET_MASK}
        sudo ipfw del ${IPRULENUM}
      fi
  elif [[ "${OSTYPE}" =~ ^freebsd.*$ ]]; then
      echo "detected FreeBSD os.."
      ##OSX
      if [[ "`ifconfig em0 | grep ${SRV_IP}`xxx" != "xxx" ]]; then
        echo "Removing private ip: ${SRV_IP}"
        sudo ifconfig em0 inet -alias ${SRV_IP} netmask ${NET_MASK}
        sudo ipfw del ${IPRULENUM}
      fi
  elif [[ ${OSTYPE} =~ ^linux.*$ ]]; then
    echo "detected linux"
    echo "sudo ip addr del $SRV_IP/$NET_MASK dev $NET_DEV label $NET_LABEL"
    sudo ip addr del $SRV_IP/$NET_MASK dev $NET_DEV label $NET_LABEL
  fi

else
    export ${ENV_CNTR_VAR}=$((${DEREF_CNTR_VAR} - 1 ))
    eval DEREF_CNTR_VAR=\$${ENV_CNTR_VAR}
    declare -p ${ENV_CNTR_VAR} | cut -d " " -f 3- > ${VIRTUALENV_STATUS}
    echo "There are still some virtual_env's active, running: ${DEREF_CNTR_VAR}"
fi
###############################################################################